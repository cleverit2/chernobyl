# Herramientas y librerias utilizadas:
# Cucumber: 
Es una herramienta que apoya el desarrollo impulsado por el comportamiento (BDD). Cucumber lee las especificaciones ejecutables escritas en texto plano, en un lenguaje de alto nivel y valida que el software hace lo que dicen esas especificaciones. Las especificaciones constan de varios ejemplos o escenarios. 
# Selenium: 
Es un entorno de pruebas que se utiliza para comprobar si el software que se está desarrollando funciona correctamente. Esta herramienta permite: grabar, editar y depurar casos de pruebas que se pueden automatizar. Lo interesante de Selenium es que se pueden editar acciones o crearlas desde cero.
# Junit: 
Se trata de un Framework Open Source para la automatización de las pruebas (tanto unitarias, como de integración) en los proyectos Software. El framework provee al usuario de herramientas, clases y métodos que le facilitan la tarea de realizar pruebas en su sistema y así asegurar su consistencia y funcionalidad.

# Plan de pruebas

Se encuentra en la raiz del proyecto en un archivo excel y su nombre es PlandePruebas

# Evidencias

Se encuentran en la raiz del proyecto en una carpeta con nombre Evidencias

# Qué necesitamos para ejecutar nuestro proyecto de forma local:
1. Java JDK
2. Maven
3. Gheckodriver

# Pasos para ejecutar la prueba online:

1. Abrir la consola de git
2. Posicionarse en la carpeta raiz del proyecto (cd chernobyl)
3. Ejecutar la instruccion "mvn test"

