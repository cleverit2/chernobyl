package ChernobylTest;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(glue = {"ChernobylTest"},
        features = "src/test/resources/AFPCapital.feature",
        tags = {"@Register or @Login"}
)


public class RunCucumberTest {

}