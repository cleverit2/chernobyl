package ChernobylTest;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Instant;

public class ChernobylTest {

    WebDriver driver = new FirefoxDriver();

  @Given("I have ChernobylTest url")
public void i_have_ChernobylTest_url() {
      driver.get("https://sura-qa-chernobyl-test.stackblitz.io/account/register");
}

@When("Select the run")
public void select_the_run() throws InterruptedException {
    WebDriverWait wait = new WebDriverWait(driver, 35);
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[4]/div/button/span")));
      driver.findElement(By.xpath("/html/body/div[4]/div/button/span")).click();
      Thread.sleep(5000);
}

@When("Select the title {string} list")
public void select_the_title_list(String Title) {
    WebElement identifier = driver.findElement(By.xpath("/html/body/div/div/div/div/div/div/form/div/div[1]/div[1]/select"));
    Select select = new Select(identifier);
    select.selectByVisibleText("Miss");
}

@When("I write First Name {string}")
public void i_write_First_Name(String FirstName) {
    driver.findElement(By.xpath("/html/body/div/div/div/div/div/div/form/div/div[1]/div[2]/input")).sendKeys(FirstName);
}

@When("I write Last Name {string}")
public void i_write_Last_Name(String LastName) {
   driver.findElement(By.xpath("/html/body/div/div/div/div/div/div/form/div/div[1]/div[3]/input")).sendKeys(LastName);
}

@When("I write Email {string}")
public void i_write_Email(String Email) {
    driver.findElement(By.xpath("/html/body/div/div/div/div/div/div/form/div/div[2]/input")).sendKeys(Email);
}

@When("I write Password {string}")
public void i_write_Password(String Password) {
    driver.findElement(By.xpath("/html/body/div/div/div/div/div/div/form/div/div[3]/div[1]/input")).sendKeys(Password);
}

@When("I write Confirm Password {string}")
public void i_write_Confirm_Password(String Password) {
    driver.findElement(By.xpath("/html/body/div/div/div/div/div/div/form/div/div[3]/div[2]/input")).sendKeys(Password);
}

@When("I press Accept Terms & Conditions")
public void i_press_Accept_Terms_Conditions() {
    driver.findElement(By.xpath("//*[@id=\"acceptTerms\"]")).click();
}

@When("I press Register")
public void i_press_Register() throws InterruptedException {
    driver.findElement(By.xpath("/html/body/div/div/div/div/div/div/form/div/div[5]/button")).click();
    Thread.sleep(8000);
}

@Then("I see Verification Email {string} in the page")
public void i_see_Verification_Email_in_the_page(String Ruta) {
   WebDriverWait wait = new WebDriverWait (driver, 15);
        String Message = "Thanks for registering!";
        Assert.assertEquals(Message, driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div/span/p[1]")).getText() );
}

@Then("I press the below link to verify your email address")
public void i_press_the_below_link_to_verify_your_email_address() throws InterruptedException {
    driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div/span/p[3]/a")).click();
    Thread.sleep(8000);
  }

@When("I write the Email1 {string}")
public void i_write_the_Email(String Email) {
    WebDriverWait wait = new WebDriverWait(driver, 60);
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div/div/div/div/div/div/form/div/div[1]/input")));
    driver.findElement(By.xpath("/html/body/div/div/div/div/div/div/form/div/div[1]/input")).sendKeys(Email);
}
@When("I write Password1 {string}")
    public void i_write_Password1(String Password) {
        driver.findElement(By.xpath("/html/body/div/div/div/div/div/div/form/div/div[2]/input")).sendKeys(Password);
    }

@When("I press Accept Login")
public void i_press_Accept_Login() throws InterruptedException {
    driver.findElement(By.xpath("/html/body/div/div/div/div/div/div/form/div/div[3]/div[1]/button")).click();
    Thread.sleep(7000);
}



}
