Feature: AFPCapital Search
  Background:

  @Register
  Scenario Outline: Register en ChernobylTest

    Given I have ChernobylTest url
    When Select the run
    And Select the title "<Title>" list
    And   I write First Name "<FirstName>"
    And   I write Last Name "<LastName>"
    And   I write Email "<Email>"
    And   I write Password "<Password>"
    And   I write Confirm Password "<Password>"
    And I press Accept Terms & Conditions
    And I press Register

    Then I see Verification Email "<Ruta>" in the page
    And  I press the below link to verify your email address
    And I write the Email1 "<Email>"
    And  I write Password1 "<Password>"
    And I press Accept Login

    Examples:
      | Title|FirstName|LastName|Email|Password|Ruta|
      |Miss|Kimberly|Herrera|kherrera@cleveritgroup.com|1234567|https://sura-qa-chernobyl-test.stackblitz.io/account/login|

